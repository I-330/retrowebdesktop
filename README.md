# RetroWebDesktop

## About

The goal of this project is to complete a fully featured and configurable
desktop environment which is fully contained within the web browser. It should
be very CPU efficient, require little to no external resources, and have a sleek
and fast design throughout. 

It should be easy to deploy and configure, and it should be simple to develop
new desktops/apps/and themes with minimal effort. All apps should function
cross-desktop with global theme variables imported and used by each app
respectfully (i.e. buttons, navbars, fonts, etc)

There should be as little reliance on PHP & server-side code as possible, only
maybe to be used in order to avoid the use of external resources. All files and
potential user data should be stored in cookies, and the site should be able to
function at some degree with javascript and css disabled. (Even if
'functionality' just means a separate non-fully-featured page displayed for 
those users).

Once completed, the code should continue to remain open source, and should be
free to use and distribute (excluding for-profit use cases which make more than
X amount of money). I haven't found a license that I like just yet, but a
suitable license will be added before the first major release.

## To-Do

- [ ] Refactor Win95 Theme & Codebase
- [ ] Develop cross-theme framework
- [ ] modularize the codebase & themes
- [ ] Create an app system
- [ ] Config file support
- [ ] KDE1.0 Theme
- [ ] Solaris CDE Theme
- [ ] Win XP/Vista Theme
- [ ] Add mouse select & icon movement support
- [ ] Mobile Support
- [ ] Window/Desktop collision
- [ ] Login/Logout page
- [ ] Splash Screen
- [ ] Minimize CSS/JS
- [ ] Optimize assets for web
- [ ] Refactor JS for CPU performance
- [ ] Create App Catalogue (web,files,notepad,games,semi-persistent files using cookies, Upload/Download files)
- [ ] Get rid of ALL external resources (including external iframes if possible)
- [ ] Dynamic page resolution for extra large and extra small display resolutions
- [ ] In-App desktop configurations (use cookies)
- [ ] Maybe change license to something which requires paid license to profit over certain amount from framework
- [ ] Dark Modes


