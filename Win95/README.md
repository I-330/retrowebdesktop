# Windows 95 Web Desktop Theme

The idea behind this is to somewhat accurately emulate the look, feel, and
functionality of the Windows 95 Desktop operating system with nothing more than
html, css, and javascript. The design should be highly modular, making it easy
to add new icons, windows, buttons, and functionality to the desktop without
having to write much code. The design is also ideally built to be highly
dynamic, supporting most aspect ratios and resolutions, with the current
exception of mobile. Global themes and variables should be used as often as
possible, and exceptions to rules should be duley noted with comments.
