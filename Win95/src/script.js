
function toggle_window(window_id) {
  if (document.getElementById(window_id + '_window').style.visibility == "hidden"){
    open_window(window_id);
    document.getElementById(window_id + "_btn").style.borderStyle = "inset";
  }else{
    minimize_window(window_id);
    document.getElementById(window_id + '_btn').style.borderStyle = "outset";
  }
}

function load_window(window_id) {
	// Create Iframe of stream
  var window_check = document.getElementById(window_id + '_iframe');
  if (window_check != null){
    document.getElementById(window_id + '_icon').style.border = "1px dotted transparent";
    return;
  }

	var stream_iframe = document.createElement("iframe");
	var stream_url = "http://198.54.128.124:31593/" + window_id;
  var draggable_window_id = window_id + '_window';
	stream_iframe.setAttribute("src", stream_url);
	stream_iframe.setAttribute("id", window_id + "_iframe");
	
  document.getElementById(window_id + '_btn').style.display = "inline";

  document.getElementById(draggable_window_id).appendChild(stream_iframe);
  
  document.getElementById(draggable_window_id).style.visibility = "visible";

  document.getElementById(window_id + '_icon').style.border = "1px dotted transparent";

  document.getElementById(window_id + "_btn").style.borderStyle = "inset";

}

function minimize_window(window_id) {
  document.getElementById(window_id + '_window').style.visibility = "hidden";
  document.getElementById(window_id + "_btn").style.borderStyle = "outset";
}

function open_window(window_id) {
  document.getElementById(window_id + '_window').style.visibility = "visible";
  document.getElementById(window_id + "_btn").style.borderStyle = "inset";
}

function unload_window(window_id) {
	const stream_iframe = document.getElementById(window_id + "_iframe");
	stream_iframe.remove();

  var draggable_window_id = window_id + '_window';
  document.getElementById(window_id + '_btn').style.display = "none";
  document.getElementById(draggable_window_id).style.visibility = "hidden";
}

function activate_border(window_id) {


  switch(window_id) {
    case 'dubz':
      document.getElementById('dubz_icon').style.border = "1px dotted white";
		  document.getElementById('drumz_icon').style.border = "1px dotted transparent";
		  document.getElementById('rapz_icon').style.border = "1px dotted transparent";
  		break;

    case 'drumz':
      document.getElementById('drumz_icon').style.border = "1px dotted white";
		  document.getElementById('dubz_icon').style.border = "1px dotted transparent";
		  document.getElementById('rapz_icon').style.border = "1px dotted transparent";
      break;
  	
    case 'rapz':
      document.getElementById('rapz_icon').style.border = "1px dotted white";
		  document.getElementById('drumz_icon').style.border = "1px dotted transparent";
		  document.getElementById('dubz_icon').style.border = "1px dotted transparent";
  		break;
    
    default:
      break;
  } 
}

































//Make the DIV element draggagle:
var items = document.getElementsByClassName("dragdiv");

for (var i = 0; i < items.length; ++i) {
  centerElement(items[i],i*30);
  dragElement(items[i]);
}

function dragElement(elmnt) {
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  elmnt.childNodes[1].onmousedown = dragMouseDown;

  function dragMouseDown(e) {
    e = e || window.event;
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e) {
    e = e || window.event;
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    
    //jquery z-index management
    elmnt.style.zIndex = items.length;
    for (var i = 0; i < items.length; ++i) {
      if(i !=  $(elmnt).index() ) {
        items[i].style.zIndex = elmnt.style.zIndex - 1;
      }
    }
  }

  function closeDragElement() {
    /* stop moving when mouse button is released:*/
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

function centerElement(elmnt, offset) {
  elmnt.style.top = document.body.clientHeight/2 - elmnt.clientHeight/2 - offset + "px";;
  elmnt.style.left = document.body.clientWidth/2 - elmnt.clientWidth/2 + offset +"px";
}
